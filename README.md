# Implementing Hedera Hashgraph Consensus, fair fast BFT in DistAlgo
<https://sites.google.com/a/stonybrook.edu/sbcs535/projects/hashgraph-distalgo>

_Update: In order to perform the hashing the program is tentatively using pysodium._

## Dependencies

- Python v3.6.5
- DistAlgo v1.1
- [pysodium](https://pypi.python.org/pypi/pysodium)

## Installing pysodium

Pysodium is a simple wrapper around LibSodium renamed as NaCl.

This wrapper requires a pre-installed libsodium from:

<https://github.com/jedisct1/libsodium>

1. Compilation on Unix-like systems
- Download a tarball of libsodium, preferably the latest stable version, then follow the ritual:
- Follow:
    ```
    ./configure
    make && make check
    sudo make install
    ```
2. Compilation on Windows
- git clone https://github.com/jedisct1/libsodium.git
- Downloaded the latest libsodium-x-mingw.tar.gz from https://download.libsodium.org/libsodium/releases/
- Extract the files.
- copy libsodium-win64\bin\libsodium-18.dll into C:\Windows\System32 as libsodium.dll.
